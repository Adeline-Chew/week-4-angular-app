import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BoardService } from '../board/board.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  regForm: FormGroup;
  headline: string;

  constructor(private bServ: BoardService, private router: Router) {
    this.regForm = new FormGroup({
      player1: new FormControl(''),
      player2: new FormControl(''),
    });
    this.headline = '';
  }

  ngOnInit(): void {}

  onSubmit() {
    this.bServ.registerPlayers(
      this.regForm.value.player1,
      this.regForm.value.player2
    );
    this.router.navigate(['/board']);
  }
}
